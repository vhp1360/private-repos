<div dir="rtl" align="right">بنام خدا</div>
- Nexus[#nexus]


# Nexus
## Repos
### Docker
because we couldn't url like _110.110.110.110/repository/docker-group_ for **docker daemon**, we don't need to **docker group** for just one **docker proxy** and **docker host**.
- it is possible to setup both on **http** type
#### general configs for docker
1. add **Docker Bearer Token Realm** in _Security|Realms_ menue to _Active_ part.
2. create Role by all _docker view access_
3. create user and grant above role to it.(called _docker_ user)
#### docker proxy
on server: set port for **HTTP** section on configuration page.(e.g. 8088)
### docker host
on server: set port for **HTTP** section on configuration page.(e.g. 8087)
### client config
1. put below in `/etc/docker/daemon.json`:
```json
{
    "registry-mirrors": ["http://privateRepo:8088"],
    "insecure-registries" : [ "http://privateRepo:8087"] 
}
```
2. login to your private repo with **docker** user wich created above for _pushing_.
3. to pushing an image, you must first, create new tag in this type: 
```
docker tag [OPTIONS] IMAGE[:TAG] [REGISTRYHOST/][USERNAME/]NAME[:TAG]
```
    then
```
docker push [REGISTRYHOST/][USERNAME/]NAME[:TAG]
```
